<?php
	require_once("connexion.php"); //require_once : charge connexion si ça n'existe pas en mémoire sinon rien ne se passe
	                               // require = include 
	$bd = new Connexion();                               
	$requete = "SELECT *FROM resultat ORDER BY rang";
	//var_dump($bd->getBDD())
    $result = $bd->selectQuery($requete);
	                               
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Les Courreurs</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	  
	<div class="container marginTop">
		  <div class="panel panel-success marginTop">
			<div class="panel-heading">Rechercher des courreurs...</div>
			<div class="panel-body">Le contenu du panneau</div>
		  </div>
		  
		   <div class="panel panel-primary">
			<div class="panel-heading">Liste des courreurs</div>
			<div class="panel-body">
			    <table class = "table  table-dark table-striped table-bordered">
					<thead>
						<tr>
							<th>Numéro course</th><th>Numéro licence</th><th>Rang</th><th>Temps</th>
						</tr>
					</thead>
					<tbody>
						<?php
							while($courreur = $result->fetch()){
							
						?>
						<tr>
							<td><?php echo $courreur['numCourse']?></td>
							<td><?php echo $courreur['numLicence'] ?></td>
							<td><?php echo $courreur['rang'] ?></td>
							<td><?php echo $courreur['temp'] ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
					<tfoot>
					</tfoot>
			    </table>
			</div>
		  </div>
	</div>

	</body>
</html>
