-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 14, 2020 at 10:12 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MaBaseDeDonnees`
--

-- --------------------------------------------------------

--
-- Table structure for table `courreur`
--

CREATE TABLE `courreur` (
  `numLicence` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `dateNaissance` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courreur`
--

INSERT INTO `courreur` (`numLicence`, `nom`, `prenom`, `dateNaissance`) VALUES
(20020511, 'Py', 'Bernard', '1942-11-05'),
(20022009, 'Boe', 'Jean Marie', '1952-08-18'),
(20022011, 'Lochard', 'Eric Olivier', '1952-06-15'),
(20022911, 'Quinqueton', 'Joel', '1952-11-29');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `numCourse` int(11) NOT NULL,
  `codePostal` int(11) DEFAULT NULL,
  `ville` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`numCourse`, `codePostal`, `ville`) VALUES
(6789, 75000, 'Paris'),
(22001, 78001, 'Versailles'),
(22002, 78023, 'Versailles'),
(22003, 78021, 'Versailles'),
(22004, 75017, 'Paris'),
(22006, 75009, 'Paris'),
(22007, 33009, 'Nimes'),
(22008, 33019, 'Nimes'),
(22098, 93200, 'Montpellier'),
(23456, 75001, 'Paris'),
(34561, 75017, 'Paris'),
(200201, 34000, 'Montpellier'),
(200202, 78000, 'Versailles'),
(200203, 6000, 'Nice'),
(200204, 30000, 'Nimes'),
(200205, 66000, 'Perpignan'),
(200206, 64000, 'Biarritz');

-- --------------------------------------------------------

--
-- Table structure for table `resultat`
--

CREATE TABLE `resultat` (
  `numCourse` int(11) NOT NULL,
  `numLicence` int(11) NOT NULL,
  `temp` time DEFAULT NULL,
  `rang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resultat`
--

INSERT INTO `resultat` (`numCourse`, `numLicence`, `temp`, `rang`) VALUES
(200202, 20020511, '01:20:00', 6),
(200202, 20022009, '01:35:00', 12),
(200203, 20022010, '01:35:00', 8),
(200203, 20022011, '01:40:00', 15),
(200203, 20022911, '01:55:00', 20),
(200204, 20020511, '01:15:00', 2),
(200204, 20022911, '01:25:00', 15);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `login` varchar(50) DEFAULT NULL,
  `pwd` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `etat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `pwd`, `email`, `role`, `etat`) VALUES
(1, 'user1', '24c9e15e52afc47c225b757e7bee1f9d', 'user1@gmail.com', 0, 0),
(2, 'user2', '7e58d63b60197ceb55a1c487989a3720', 'user2@gmail.com', 0, 1),
(3, 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'admin1@gmail.com', 1, 1),
(4, 'admin2', 'c84258e9c39059a89ab77d846ddab909', 'admin2@gmail.com', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courreur`
--
ALTER TABLE `courreur`
  ADD PRIMARY KEY (`numLicence`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`numCourse`);

--
-- Indexes for table `resultat`
--
ALTER TABLE `resultat`
  ADD PRIMARY KEY (`numCourse`,`numLicence`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
