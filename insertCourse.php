<?php
    require_once("maSessionIdentifier.php"); // On n'accéde pas à la page sans identification
	require_once("connexion.php");
	$bd = new Connexion();
	if(isset($_POST['ville'])){
		$ville = $_POST['ville'];
		$codePostal = $_POST['codePostal'];
		$codeCourse = $_POST['codeCourse'];
		$requete = "INSERT INTO course(numCourse, ville, codePostal) VALUES(?,?,?)";
		$params = array($codeCourse,$ville, $codePostal);
		$bd->updateQuery($params,$requete);
		header("location: course.php");
	}
?>
