<?php
	if(isset($_GET['message'])){
		$msg = $_GET['message'];
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Alerte</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	<div class="container marginTop">
		   <div class="panel panel-danger marginTop60">
			<div class="panel-heading"><h4>Erreur:</h4></div>
			<div class="panel-body">
				<h3><?php echo $msg ?></h3>
				<h4><a href = "<?php echo $_SERVER['HTTP_REFERER']; //PAGE PRECEDENTE?>">Retour >>>></a></h4>
			</div>
		  </div>
	</div>
	</body>
</html>
