<?php
    class Connexion{
		var $db_host;
		var $db_name;
		var $db_user;
		var $db_pass;
		var $bdd;
		
		function __construct($db_host = "127.0.0.8",$db_name = "MaBaseDeDonnees", $db_user = "root",$db_pass = "") {
			$this->db_host = $db_host;
			$this->db_name = $db_name;
			$this->db_user = $db_user;
			$this->db_pass = $db_pass;
		    try{
		       $this->bdd = new PDO("mysql:host =".$db_host.";dbname=".$db_name, $db_user, $db_pass);
		       //echo "connection réussie "; 
	         }
	         catch(PDOException $e){
				echo " Erreur de connexion <br>";
				die($e->getMessage());
			 }
		}
		
		function getBDD(){
			return $this->bdd;
		}
		
		 function selectQuery($query){
				$result = $this->bdd->query($query);
				return $result;	
         }
         
         function updateQuery($params,$query){
			 $result =$this->bdd->prepare($query);
			 $result->execute($params);
		 }
	 }
?>
