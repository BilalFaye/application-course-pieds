<?php
	require_once("connexion.php"); //require_once : charge connexion si ça n'existe pas en mémoire sinon rien ne se passe
	                               // require = include 
	$bd = new Connexion();                               
	$requete = "SELECT *FROM courreur";
	//var_dump($bd->getBDD())
    $result = $bd->selectQuery($requete);
	                               
?>
<?php
    require_once("maSessionIdentifier.php"); // On met les variables de session dans une page
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Les Courreurs</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	  
	<div class="container marginTop">
		  <div class="panel panel-success marginTop">
			<div class="panel-heading">Rechercher des courreurs...</div>
			<div class="panel-body">Le contenu du panneau</div>
		  </div>
		  
		   <div class="panel panel-primary">
			<div class="panel-heading">Liste des courreurs</div>
			<div class="panel-body">
			    <table class = "table  table-striped table-bordered">
					<thead>
						<tr>
							<th>Numero Licence</th><th>Nom</th><th>Prenom</th><th>Date de naissance</th>
						</tr>
					</thead>
					<tbody>
						<?php
							while($courreur = $result->fetch()){
							
						?>
						<tr>
							<td><?php echo $courreur['numLicence']?></td>
							<td><?php echo $courreur['nom'] ?></td>
							<td><?php echo $courreur['prenom'] ?></td>
							<td><?php echo $courreur['dateNaissance'] ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
					<tfoot>
					</tfoot>
			    </table>
			</div>
		  </div>
	</div>

	</body>
</html>
