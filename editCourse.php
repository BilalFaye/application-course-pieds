<?php
    require_once("maSessionIdentifier.php"); // On n'accéde pas à la page sans identification
	require_once("connexion.php");
	$bd = new Connexion();
	if(isset($_GET['idC'])){
		$idCourse = $_GET['idC'];
		$requete = "SELECT *FROM course WHERE numCourse = $idCourse";
		$result = $bd->selectQuery($requete);
		$course = $result->fetch();
		$ville = $course['ville'];
		$codePostal = $course['codePostal'];
	}
?>
<!DOCTYPE html>

<html>
	<head>
		<title>Ajouter Filière</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	  
	<div class="container marginTop">
		   <div class="panel panel-primary">
			<div class="panel-heading">Mise à jour course</div>
			<div class="panel-body">
				<form method ="post" action = "updateCourse.php" class = "form">
					<div class = "form-group">
				        <label for="numero">Numéro course: <?php echo $idCourse; ?></label>
						<input type = "hidden" name ="codeCourse"  class = "form-control" id ="numero" value= "<?php echo $idCourse; ?>"> 
					</div> 	
						<label for="niveau">Ville:</label>
						<select name ="ville" class = "form-control" id ="niveau">
							<option value="Montpellier" <?php if($ville==="Montpellier") echo "selected" ?>>Montpellier</option>
							<option value="Versailles" <?php if($ville==="Versailles") echo "selected" ?>>Versailles</option>
							<option value="Nice" <?php if($ville==="Nice") echo "selected" ?>>Nice</option>
							<option value="Nimes" <?php if($ville==="Nimes") echo "selected" ?>>Nimes</option>
							<option value="Perpignan" <?php if($ville==="Perpignan") echo "selected" ?>>Perpignan</option>
							<option value="Biarritz" <?php if($ville==="Biarritz") echo "selected" ?>>Biarritz</option>
							<option value="Paris" <?php if($ville==="Paris") echo "selected" ?>>Paris</option>
						</select>
				       <div class = "form-group">
				           <label for="code">Code postal:</label>
						   <input type = "number" name ="codePostal" placeholder = "Code postal" class = "form-control" id ="code" value="<?php echo $codePostal; ?>"> 
					   </div> 
						<button type="submit" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class = "glyphicon glyphicon-save"></span>Valider</button>
				</form>
			</div>
		  </div>
	</div>

	</body>
</html>
