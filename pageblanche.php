<!DOCTYPE html>
<html>
	<head>
		<title>Les filières</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	  
	<div class="container marginTop">
		  <div class="panel panel-success marginTop">
			<div class="panel-heading">Panel Heading</div>
			<div class="panel-body">Panel Content</div>
		  </div>
		  
		   <div class="panel panel-primary">
			<div class="panel-heading">Panel Heading</div>
			<div class="panel-body">Panel Content</div>
		  </div>
	</div>

	</body>
</html>
