<!DOCTYPE html>
<html>
	<head>
		<title>Ajouter Filière</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	  
	<div class="container marginTop">
		   <div class="panel panel-primary">
			<div class="panel-heading">Saisi des données de la nouvelle course</div>
			<div class="panel-body">
				<form method ="post" action = "insertCourse.php" class = "form">
					<div class = "form-group">
				        <label for="numero">Numéro course:</label>
						<input type = "text" name ="codeCourse" placeholder = "Numéro de la course" class = "form-control" id ="numero" required> 
					</div> 	
						<label for="niveau">Ville:</label>
						<select name ="ville" class = "form-control" id ="niveau">
							<option value="Montpellier">Montpellier</option>
							<option value="Versailles">Versailles</option>
							<option value="Nice">Nice</option>
							<option value="Nimes">Nimes</option>
							<option value="Perpignan">Perpignan</option>
							<option value="Biarritz">Biarritz</option>
							<option value="Paris">Paris</option>
						</select>
				       <div class = "form-group">
				           <label for="code">Code postal:</label>
						   <input type = "number" name ="codePostal" placeholder = "Code postal" class = "form-control" id ="code" required> 
					   </div> 
						<button type="submit" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class = "glyphicon glyphicon-save"></span>Enrégistrer</button>
				</form>
			</div>
		  </div>
	</div>

	</body>
</html>
