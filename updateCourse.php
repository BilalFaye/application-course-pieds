<?php
    require_once("maSessionIdentifier.php"); // On n'accéde pas à la page sans identification
	require_once("connexion.php");
	$bd = new Connexion();
	if(isset($_POST['ville'])){
		$ville = $_POST['ville'];
		$codePostal = $_POST['codePostal'];
		$codeCourse = $_POST['codeCourse'];
		$requete = "UPDATE course SET ville=?, codePostal =? WHERE numCourse =?";
		$params = array($ville, $codePostal, $codeCourse);
		$bd->updateQuery($params,$requete);
		header("location: course.php");
	}
?>
