<?php
	require_once("maSessionIdentifier.php");
	require_once("connexion.php");
	$db = new Connexion();
	$id = (isset($_GET['idU']))? $_GET['idU']:0;
	$requete = "DELETE FROM utilisateur WHERE id = ?";
    $params = array($id);
    $db->updateQuery($params, $requete);
    header('location: utilisateur.php');
?>
