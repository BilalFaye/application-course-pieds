<?php
    require_once("maSessionIdentifier.php"); // On n'accéde pas à la page sans identification
	require_once("connexion.php"); //require_once : charge connexion si ça n'existe pas en mémoire sinon rien ne se passe
	                               // require = include  
	                               // le selected permet de faire la recherche sans appuyer le bouton rechercher
	                               // La pagination peut se faire coté serveur comme coté client
	                               
	                               /*Pour continuer
	                                * la modification créee un formulaire dans une autre page 
	                                * la suppresion dirige vers une page php qui supprime et redirige vers utilisateur
	                                * changer l'etat dirige vers une page php qui redirige vers utilisateur
	                                */ 
	$bd = new Connexion();                               
	//var_dump($bd->getBDD())
    $login = isset($_GET['login'])?$_GET['login']:"";
    $size = isset($_GET['nbPage'])?$_GET['nbPage']:3; // gestion du nombre de course à prendre dans la base avec limit    
    $page = isset($_GET['numeroPage'])?$_GET['numeroPage']:1;
    $offset = ($page-1)*$size;
    if($login ==""){
    /*
     * voir LIKE %%
     */ 
        $requete = "SELECT *FROM utilisateur limit $size offset $offset";
        $requeteCount = "SELECT count(*) as countC FROM utilisateur";// Gestion de la pagination
    }    
    else{
		$requete = "SELECT *FROM utilisateur WHERE login = '$login' limit $size offset $offset";
        $requeteCount = "SELECT count(*) as countC FROM utilisateur WHERE login = '$login'";// Gestion de la pagination
	}
    $result = $bd->selectQuery($requete);  
    $resultCount = $bd->selectQuery($requeteCount);
    $resultTabCount = $resultCount->fetch();
    $nbUtilisateurs = $resultTabCount['countC'] ;
	if($nbUtilisateurs%$size == 0){ // On gére le nombre de page à créer 
		$nbPage = $nbUtilisateurs/$size;
	}
	else
	    $nbPage =  floor($nbUtilisateurs/$size) + 1; // La partie entère plus 1                              
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Les Courreurs</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	  
	<div class="container marginTop">
		  <div class="panel panel-success marginTop">
			<div class="panel-heading">Rechercher des utilisateurs...</div>
			<div class="panel-body">
				<form method ="get" action = "utilisateur.php" class = "form-inline">
					<div class = "form-group">
						<input type = "text" name ="login" placeholder = "login" class = "form-control"> 
					</div> 	 
						<button type="submit" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class = "glyphicon glyphicon-search"></span>Chercher...</button>
				
				</form>
			</div>
		  </div>
		  
		   <div class="panel panel-primary">
			<div class="panel-heading">Liste des utilisateurs (<?php echo $nbUtilisateurs?>) utilisateurs</div>
			<div class="panel-body">
			    <table class = "table  table-striped table-bordered">
					<thead>
						<tr>
							<th>Login</th><th>Email</th><th>Role</th><th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
						    if($result){ // $result est un booleen true si la table n'est pas vie et false sinon
								while($utilisateur = $result->fetch()){
								
							?>
							<tr class ="<?if($utilisateur['etat'] == 1) echo 'success'; else echo 'danger';?>">
								<td><?php echo $utilisateur['login']?></td>
								<td><?php echo $utilisateur['email'] ?></td>
								<td><?php if($utilisateur['role'] == 1) echo "admin"; else echo "user" ?></td>
								<td>
									<a href = "editUtilisateur.php?idU=<?php echo $utilisateur['id'] ?>" >
										<span class = "glyphicon glyphicon-edit"></span>
					
									</a>
									&nbsp;
									<a  onclick = "return confirm('Êtes-vous sûr de vouloir supprimer cet utilisateur?');" href ="supprimerUtilisateurs.php?idU=<?php echo $utilisateur['id'] ?>">
										<span class = "glyphicon glyphicon-trash"></span>
									</a>
									&nbsp;
									<a href = "activerUser.php?idU=<?php echo $utilisateur['id']?>" >
										<?php
											if($utilisateur['etat'] == 1)
												echo '<span class = "glyphicon glyphicon-ok"></span>';
											else 
												echo '<span class = "glyphicon glyphicon-remove"></span>';

										?>
								    </a>
								</td>
							</tr>
							<?php
							}
					    }
						?>
					</tbody>
			    </table>
			    <div>
					<ul class ="pagination">
					<?php for($i=1; $i <= $nbPage; $i++) { ?> 
						<li class = "<?php if ($i == $page) echo 'active'; ?>"><a href = "utilisateur.php?numeroPage=<?php echo $i; ?>"><?php echo $i; ?></a></li>
  
					<?php } ?>	
					</ul>
			    </div>
			</div>
		  </div>
	</div>

	</body>
</html>
