<?php
    require_once("maSessionIdentifier.php"); // On n'accéde pas à la page sans identification
	require_once("connexion.php"); //require_once : charge connexion si ça n'existe pas en mémoire sinon rien ne se passe
	                               // require = include  
	                               // le selected permet de faire la recherche sans appuyer le bouton rechercher
	                               // La pagination peut se faire coté serveur comme coté client
	$bd = new Connexion();                               
	//var_dump($bd->getBDD())
    $text =isset($_GET['searchText'])?$_GET['searchText']:"";
    $ville = isset($_GET['villeSelected'])?$_GET['villeSelected']:"all";
    $size = isset($_GET['nbPage'])?$_GET['nbPage']:7; // gestion du nombre de course à prendre dans la base avec limit    
    $page = isset($_GET['numeroPage'])?$_GET['numeroPage']:1;
    $offset = ($page-1)*$size;
    if($ville =="all" and $text == ""){
    /*
     * voir LIKE %%
     */ 
        $requete = "SELECT *FROM course limit $size offset $offset";
        $requeteCount = "SELECT count(*) as countC FROM course";// Gestion de la pagination
    }    
    else
		if($ville == "all" and $text != ""){
			$requete = "SELECT *FROM course WHERE numCourse = $text limit $size offset $offset";
			$requeteCount = "SELECT count(*) as countC FROM course WHERE numCourse = $text";
		}
		else	
			if($ville !="all" and $text == ""){
		        $requete = "SELECT *FROM course WHERE ville = '$ville' limit $size offset $offset";
				$requeteCount = "SELECT count(*) as countC FROM course WHERE ville = '$ville'" ;
			}
		        else{
					$requete = "SELECT *FROM course WHERE ville = '$ville' AND numCourse = $text limit $size offset $offset";
					$requeteCount = "SELECT count(*) as countC FROM course WHERE ville = '$ville' AND numCourse = $text";
				}
    $result = $bd->selectQuery($requete);  
    $resultCount = $bd->selectQuery($requeteCount);
    $resultTabCount = $resultCount->fetch();
    $nbCount = $resultTabCount['countC'] ;
	if($nbCount%$size === 0){ // On gére le nombre de page à créer 
		$nbPage = $nbCount/$size;
	}
	else
	    $nbPage =  floor($nbCount/$size) + 1; // La partie entère plus 1                              
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Les Courreurs</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
		<?php include("menu.php");?>
	  
	<div class="container margin">
		  <div class="panel panel-success marginTop">
			<div class="panel-heading">Rechercher des courreurs...</div>
			<div class="panel-body">
				<form method ="get" action = "course.php" class = "form-inline">
					<div class = "form-group">
						<input type = "text" name ="searchText" placeholder = "Taper le numéro du courreur" class = "form-control"> 
					</div> 	
						<label for="niveau">Ville:</label>
						<select name ="villeSelected" class = "form-control" id ="niveau" onchange = "this.form.submit()">
							<option value="all"<?php if($ville==="all") echo "selected" ?>>Toutes les villes</option>
							<option value="Montpellier" <?php if($ville==="Montpellier") echo "selected" ?>>Montpellier</option>
							<option value="Versailles" <?php if($ville==="Versailles") echo "selected" ?>>Versailles</option>
							<option value="Nice" <?php if($ville==="Nice") echo "selected" ?>>Nice</option>
							<option value="Nimes" <?php if($ville==="Nimes") echo "selected" ?>>Nimes</option>
							<option value="Perpignan" <?php if($ville==="Perpignan") echo "selected" ?>>Perpignan</option>
							<option value="Biarritz" <?php if($ville==="Biarritz") echo "selected" ?>>Biarritz</option>
							<option value="Paris" <?php if($ville==="Paris") echo "selected" ?>>Paris</option>
						</select>
						<button type="submit" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class = "glyphicon glyphicon-search"></span>Chercher...</button>
							&nbsp;&nbsp;
							<a href="nouvelleCourse.php"><span class = "glyphicon glyphicon-plus"></span>Nouvelle course</a>
				</form>
			</div>
		  </div>
		  
		   <div class="panel panel-primary">
			<div class="panel-heading">Liste des courreurs (<?php echo $nbCount?>) Courses</div>
			<div class="panel-body">
			    <table class = "table  table-striped table-bordered">
					<thead>
						<tr>
							<th>Numero course</th><th>Ville</th><th>Code postal</th><th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php
						    if($result){ // $result est un booleen true si la table n'est pas vie et false sinon
								while($course = $result->fetch()){
								
							?>
							<tr>
								<td><?php echo $course['numCourse']?></td>
								<td><?php echo $course['ville'] ?></td>
								<td><?php echo $course['codePostal'] ?></td>
								<td>
									<a href = "editCourse.php?idC=<?php echo $course['numCourse'] ?>" >
										<span class = "glyphicon glyphicon-edit">
									</a>
									&nbsp;
									<a  onclick = "return confirm('Êtes-vous sûr de vouloir supprimer cette course?');" href ="supprimerCourse.php?idC=<?php echo $course['numCourse'] ?>">
										<span class = "glyphicon glyphicon-trash">
									</a>
								</td>
							</tr>
							<?php
							}
					    }
						?>
					</tbody>
			    </table>
			    <div>
					<ul class ="pagination">
					<?php for($i=1; $i <= $nbPage; $i++) { ?> 
						<li class = "<?php if ($i == $page) echo 'active'; ?>"><a href = "course.php?numeroPage=<?php echo $i; ?>"><?php echo $i; ?></a></li>
					<?php } ?>	
					</ul>
			    </div>
			</div>
		  </div>
	</div>

	</body>
</html>
