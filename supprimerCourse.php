<?php
    require_once("maSessionIdentifier.php"); // On n'accéde pas à la page sans identification
	require_once("connexion.php");
	$bd = new Connexion();
	if(isset($_GET['idC'])){
		$codeCourse = $_GET['idC'];
		$requeteCount = "SELECT count(*) as nb FROM resultat WHERE numCourse = $codeCourse";
		$result = $bd->selectQuery($requeteCount);
		$nb = $result->fetch();
		if($nb['nb'] == 0){ // Aucun résultat ne dépend de cette course
			$requete = "DELETE FROM course WHERE numCourse =?";
			$params = array($codeCourse);
			$bd->updateQuery($params,$requete);
			header("location: course.php");
	    }
	    else{
			$msg = "Veuillez d'abord supprimer les résultats qui dépendent de cette course dans la table résultat!";
			header("location: alerte.php?message=$msg");
		}
	}
?>
