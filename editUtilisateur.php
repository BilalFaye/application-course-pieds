<?php
	require_once("maSessionIdentifier.php");
	require_once("connexion.php");
	
    $idUtilisateur = (isset($_GET['idU']))?$_GET['idU']:0;	
    $db = new Connexion();
    $requete = "SELECT *FROM utilisateur WHERE id = $idUtilisateur";
    $result = $db->selectQuery($requete);
    $utilisateur = $result->fetch();
    $login = $utilisateur['login'];
    $mail = $utilisateur['email'];
    if(isset($_GET['email'])){
		$mail = $_GET['email'];
		$id = $_GET['id'];
		$requete = "UPDATE utilisateur SET email =? WHERE id =?";
		$params = array($mail, $id);
		$db->updateQuery($params, $requete); 
		header("location: utilisateur.php");
	}
    
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Editer un utilisateur</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
	  
	<div class="marginTop col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
		  
		   <div class="panel panel-primary">
			<div class="panel-heading">Se connecter</div>
			<div class="panel-body">
				<form method ="get" action = "editUtilisateur.php" class = "form">
					<label>Login: <?php echo " $login"?></label>
	                &nbsp;
	                <div class = "form-group">
						<input type = "text" name ="id" class = "form-control" value = "<?php echo $idUtilisateur; ?>" hidden>
						 
					</div>
					<div class = "form-group">
						<label for="Email">Email: </label>
						<input type = "email" name ="email" class = "form-control" value = "<?php echo $mail; ?>">
						 
					</div>
					
						<button type="submit" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class = "glyphicon glyphicon-log-in"></span>Valider</button>
				</form>
			</div>
		  </div>
	</div>

	</body>
</html>
