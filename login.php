<?php
	session_start();
	if(isset($_SESSION['erreurLogin']))
		$erreur = $_SESSION['erreurLogin'];
	else
	    $erreur ="";
    session_destroy();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Se connecter</title>
		<meta charset = "utf-8">
		 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/bootstrap.min.css">
		<link rel = "stylesheet" style = "text/css" href = "../css/mon_style.css">
	</head>
	<body>
	  
	<div class="marginTop col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
		  
		   <div class="panel panel-primary">
			<div class="panel-heading">Se connecter</div>
			<div class="panel-body">
				<form method ="post" action = "seConnecter.php" class = "form">
					<?php if(!empty($erreur)){?>
					<div class = "alert-danger">
					    <?php echo $erreur ?>
					</div>
					<?php }?>
					<label for ="login">Login:</label>
					<div class = "form-group">
						<input type = "text" name ="login"  class = "form-control" id= "login" placeholder = "login"> 
					</div> 	
					<div class = "form-group">
						<label for="password">Password: </label>
						<input type = "password" name ="password" class = "form-control" placeholder = "password"> 
					</div> 
						<button type="submit" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class = "glyphicon glyphicon-log-in"></span>Se connecter</button>
				</form>
			</div>
		  </div>
	</div>

	</body>
</html>
