<?php
	require_once("maSessionIdentifier.php");
	require_once("connexion.php");
	$id = (isset($_GET['idU']))?$_GET['idU']:0;
	$db = new Connexion();
	$requete = "SELECT etat FROM utilisateur WHERE id = $id";
	$result = $db->selectQuery($requete);
	$utilisateur = $result->fetch();
	$etat = $utilisateur['etat'];
	
	$newEtat = ($etat == 0)?1:0;
	$requete = "UPDATE utilisateur SET etat = ? WHERE id = ?";
	$params = array($newEtat, $id);
	$db->updateQuery($params, $requete);
	
	header('location: utilisateur.php');
?>
